function randInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const navbar = document.getElementById('navbar')
const btnCustomer = document.getElementById('dl-customer')
const btnAnchor = document.getElementById('no_navbar_onclick')

let noNavbar = false;

window.onload = () => {
    const r = randInt(0, 5)
    if(r === 3) {
        const head = document.getElementById('_debug_head')
        head.innerHTML = 'БазарПицца'
    }
}

window.onscroll = () => {
    const y = window.pageYOffset;
    if(y >= 300 && !noNavbar) {
        navbar.style.top = "0";
    } else {
        navbar.style.top = `-${navbar.clientHeight}px`;
    }
}

btnCustomer.onclick = () =>
    window.open('index/apk/customer.apk', '_self');
btnAnchor.onclick = () => {
    noNavbar = true;
    window.setTimeout(() => noNavbar = false, 100);
}
